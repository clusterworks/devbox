# https://github.com/gitpod-io/openvscode-server/blob/main/.gitpod.Dockerfile
FROM node:14 as openvscode-server
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        xvfb x11vnc fluxbox dbus-x11 x11-utils x11-xserver-utils xdg-utils \
        fbautostart xterm eterm gnome-terminal gnome-keyring seahorse nautilus \
        libx11-dev libxkbfile-dev libsecret-1-dev libnotify4 libnss3 libxss1 \
        libasound2 libgbm1 xfonts-base xfonts-terminus fonts-noto fonts-wqy-microhei \
        fonts-droid-fallback vim-tiny nano libgconf2-dev libgtk-3-dev twm

# https://github.com/gitpod-io/openvscode-server/blob/main/docs/development.md
RUN git clone https://github.com/gitpod-io/openvscode-server && \
    cd openvscode-server && \
    git checkout openvscode-server-v1.61.0 && \
    yarn && yarn gulp server-min

FROM ubuntu:20.04

COPY install-packages /usr/bin
ARG USERNAME=devbox
ARG TARGETARCH
ARG DEBIAN_FRONTEND=noninteractive
ARG TZ="Europe/Berlin"

# prepare extras
RUN . /etc/os-release \
    && apt-get update && apt-get -y install tzdata gnupg2 curl wget \
    && curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/x${NAME}_${VERSION_ID}/Release.key | apt-key add -  \
    && sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/x${NAME}_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list" \
    && apt-get -y update \
    && curl -sL https://deb.nodesource.com/setup_12.x | bash -

RUN yes | unminimize \
    && install-packages \
        zip \
        unzip \
        p7zip-full \
        bash-completion \
        build-essential \
        ninja-build \
        htop \
        jq \
        less \
        locales \
        man-db \
        nano \
        ripgrep \
        software-properties-common \
        sudo \
        time \
        vim \
        multitail \
        lsof \
        ssl-cert \
        zsh \
        ruby-dev \
        golang \
        nodejs \
        git \
        docker.io \
        ucspi-tcp gettext-base \
        python3 python-is-python3 \
        build-essential apt-transport-https lsb-release ca-certificates curl \
        openjdk-8-jdk openjdk-11-jdk maven \
        openssh-server \
        asciidoctor tmux   \
    && locale-gen en_US.UTF-8 \
    && update-java-alternatives -s java-1.8.0-openjdk-$TARGETARCH \
    && gem install asciidoctor-pdf pygments.rb colorls

RUN ARCH=$TARGETARCH; if [ "$ARCH" = "amd64" ]; then ARCH="x86_64";fi; \
    curl -L https://mirror.openshift.com/pub/openshift-v4/$TARGETARCH/clients/ocp/stable/openshift-client-linux.tar.gz | tar -xvzC /usr/local/bin/ \
    && curl -L https://github.com/google/go-containerregistry/releases/download/v0.6.0/go-containerregistry_Linux_$ARCH.tar.gz | tar -xvzC /usr/local/bin \
    && curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash \
    && chmod a+x /usr/local/bin/*

ENV LANG=en_US.UTF-8

# buildx support (kubeconfig and docker buildx config needed)
COPY --from=docker/buildx-bin /buildx /usr/libexec/docker/cli-plugins/docker-buildx
COPY --from=openvscode-server /server-pkg /home/openvscode-server

RUN useradd -l -u 33333 -G sudo -md /home/$USERNAME -s /usr/bin/zsh -p $USERNAME $USERNAME \
    # passwordless sudo for users in the 'sudo' group
    && sed -i.bkp -e 's/%sudo\s\+ALL=(ALL\(:ALL\)\?)\s\+ALL/%sudo ALL=NOPASSWD:ALL/g' /etc/sudoers \
    && chgrp -R 0 /home && chmod -R g=u /home
ENV HOME=/home/$USERNAME TERM=xterm-256color OPENVSCODE_SERVER_ROOT=/home/openvscode-server VISUAL=code GIT_EDITOR="code --wait" EDITOR=code
WORKDIR $HOME

USER 33333

RUN git clone https://gitlab.com/smerschjohann/dotfiles.git .dotfiles && cd .dotfiles && git remote remove origin

RUN cd .dotfiles && ./install && cd $HOME && zsh -c "source .zshrc; zinit self-update; zinit update --all"

EXPOSE 3000
ENTRYPOINT ${OPENVSCODE_SERVER_ROOT}/server.sh
